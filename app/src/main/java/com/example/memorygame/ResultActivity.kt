package com.example.memorygame

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class ResultActivity: AppCompatActivity() {
    lateinit var playButton: Button
    lateinit var menuButton: Button
    lateinit var shareButton: Button
    lateinit var resultText: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_MemoryGame)
        super.onCreate(savedInstanceState)
        val bundle: Bundle? = intent.extras
        var dificulty = bundle?.getString("dificulty").toString()
        var mode = bundle?.getString("mode").toString()
        var moviments = bundle?.getString("moviments").toString()
        var parelles = bundle?.getString("parelles").toString()
        var temps = bundle?.getString("temps").toString()

        setContentView(R.layout.puntuacio)

        resultText=findViewById(R.id.textResult)
        playButton = findViewById(R.id.playButton)
        shareButton = findViewById(R.id.shareButton)
        menuButton = findViewById(R.id.menuButton)
        var resultat=""
        if (mode.equals(resources.getStringArray(R.array.mode)[0])){
            resultat=("DIFICULTAT\n"+dificulty+"\nMode: "+mode+"\nMoviments: "+moviments+"\nTemps: "+temps)
        }else{
            if (dificulty.equals(resources.getStringArray(R.array.dificulty)[1])||
                dificulty.equals(resources.getStringArray(R.array.dificulty)[3])||
                dificulty.equals(resources.getStringArray(R.array.dificulty)[5])){
                resultat=("DIFICULTAT\n"+dificulty+"\nMode: "+mode+"\nMoviments: "+moviments+"\nTernes: "+parelles)
            }else{
                resultat=("DIFICULTAT\n"+dificulty+"\nMode: "+mode+"\nMoviments: "+moviments+"\nParelles: "+parelles)
            }
        }
        resultText.text=resultat
        shareButton.setOnClickListener {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, resultat)
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)
        }

        playButton.setOnClickListener {
            val gameIntent = Intent(this, GameActivity::class.java)
            gameIntent.putExtra("dificulty", dificulty)
            gameIntent.putExtra("mode", mode)
            startActivity(gameIntent)
        }
        menuButton.setOnClickListener {
            val menuIntent = Intent(this, MenuActivity::class.java)
            startActivity(menuIntent)
        }

    }
}