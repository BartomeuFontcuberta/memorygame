package com.example.memorygame

import android.graphics.Color
import androidx.lifecycle.ViewModel
import com.google.android.material.snackbar.Snackbar
import kotlin.random.Random

class GameViewModel : ViewModel() {
    var imatges= arrayOf(
        arrayOf(R.drawable.piramide,R.drawable.piramide_louvre,R.drawable.piramide_dvp),
        arrayOf(R.drawable.con_,R.drawable.con_valencia,R.drawable.con_dvp),
        arrayOf(R.drawable.cub,R.drawable.cub_parc,R.drawable.cub_dvp),
        arrayOf(R.drawable.ppentagonal,R.drawable.ppentagonal_farnese,R.drawable.prisma_pentagonal_dvp),
        arrayOf(R.drawable.prisma_dodecagon,R.drawable.prisma_dodecagon_sevilla,R.drawable.prisma_dodecagon_dvp),
        arrayOf(R.drawable.prisma_hexagon,R.drawable.prisma_hexagon_liverpool,R.drawable.prisma_exagon_dvp),
        arrayOf(R.drawable.prisma_oblic,R.drawable.prisma_oblic_kio,R.drawable.prisma_oblic_dvp),
        arrayOf(R.drawable.prisma_octogon,R.drawable.prisma_octogon_miquelet,R.drawable.prisma_octogon_dvp),
        arrayOf(R.drawable.prisma_quadrat,R.drawable.prisma_quadrat_gratacel,R.drawable.prisma_quadrat_dvp),
        arrayOf(R.drawable.cilindre,R.drawable.cilindre_bellver,R.drawable.cilindre_dvp),
        arrayOf(R.drawable.prisma_irreg,R.drawable.prisma_irreg_horreo,R.drawable.prisma_irregular_dvp),
        arrayOf(R.drawable.tronc_con,R.drawable.tronc_con_eivissa,R.drawable.tronc_con_dvp),
        //Final ternes
        arrayOf(R.drawable.semiesfera,R.drawable.semiesfera_alqueria),
        arrayOf(R.drawable.esfera,R.drawable.esfera_sw),
        arrayOf(R.drawable.hiperboloide,R.drawable.hiperboloide_bcn),
        arrayOf(R.drawable.paraboloide_hiperbolic,R.drawable.paraboloide_hiperbolic_valencia),
        arrayOf(R.drawable.conoide,R.drawable.conoide_escoles),
        arrayOf(R.drawable.elipsoide,R.drawable.elipsoide_beijing),
        arrayOf(R.drawable.helicoide,R.drawable.helicoide_escales),
        arrayOf(R.drawable.paraboloide,R.drawable.paraboloide_agbar)
    )
    var defaultImage=R.drawable.caratula
    var parelles=arrayListOf<Int>()
    var comprovantParelles=arrayListOf<Int>()
    var contador=0
    var moviments=0
    var parellesFetes=0
    var objectiu=2
    var parellaSeleccionada=arrayListOf<Int>()
    var cartaActiva=arrayListOf<Boolean>()
    lateinit var dificulty:String
    lateinit var mode:String
    var temps=0L
    var inicialitzat=false

    fun generarParelles(quantitat:Int){
        cartaActiva.clear()
        parelles.clear()
        comprovantParelles.clear()
        var valors= ArrayList<Int>()
        var parellesPosibles= ArrayList<Int>()
        for (i in 0..imatges.size-1) {
            if (imatges[i].size>=objectiu){
                parellesPosibles.add(i)
            }else{
                break
            }
        }
        for (i in 0..quantitat-1) {
            val extreu= parellesPosibles[Random.nextInt(0,parellesPosibles.size)]
            valors.add(objectiu*extreu)
            valors.add(objectiu*extreu+1)
            if (objectiu==3){
                valors.add(objectiu*extreu+2)
            }
            parellesPosibles.remove(extreu)
        }
        for (i in 0..quantitat*objectiu-1){
            val extreu= valors[Random.nextInt(0,valors.size)]
            comprovantParelles.add(extreu/objectiu)
            parelles.add(imatges[extreu/objectiu][extreu%objectiu])
            valors.remove(extreu)
            cartaActiva.add(false)
        }
    }

    fun comprovarParella():Boolean{
        if (objectiu==2) {
            if (comprovantParelles[parellaSeleccionada[0]] == comprovantParelles[parellaSeleccionada[1]]) {
                return true
            } else {
                cartaActiva[parellaSeleccionada[0]] = false
                cartaActiva[parellaSeleccionada[1]] = false
                return false
            }
        }else{
            if (comprovantParelles[parellaSeleccionada[0]] == comprovantParelles[parellaSeleccionada[1]]
                && comprovantParelles[parellaSeleccionada[0]] == comprovantParelles[parellaSeleccionada[2]]) {
                return true
            } else {
                cartaActiva[parellaSeleccionada[0]] = false
                cartaActiva[parellaSeleccionada[1]] = false
                cartaActiva[parellaSeleccionada[2]] = false
                return false
            }
        }
    }

    fun isCartaActiva(posicio:Int):Boolean{
        return cartaActiva[posicio]
    }

    fun isMoveFinish():Boolean{
        return contador==objectiu
    }

    fun resetMove(){
        parellaSeleccionada.clear()
        contador = 0
    }

    fun girarCarta(posicio:Int):Int{
        cartaActiva[posicio]=true
        contador++
        return parelles[posicio]
    }
    fun isFinal():Boolean{
        var final = true
        for (ok:Boolean in cartaActiva){
            if (!ok){
                final=false
                break
            }
        }
        return final
    }
}