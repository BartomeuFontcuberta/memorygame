package com.example.memorygame

import android.content.Intent
import android.os.*
import android.widget.Chronometer
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.lifecycle.ViewModelProvider

class GameActivity: AppCompatActivity() {
    lateinit var movimentsText: TextView
    lateinit var parellesText: TextView
    lateinit var timer: Chronometer
    var memoryCards=arrayListOf<CardView>()
    var memoryImages=arrayListOf<ImageView>()

    private lateinit var viewModel: GameViewModel

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_MemoryGame)
        super.onCreate(savedInstanceState)
        val bundle: Bundle? = intent.extras
        viewModel= ViewModelProvider(this).get(GameViewModel::class.java)
        viewModel.dificulty = bundle?.getString("dificulty").toString()
        viewModel.mode = bundle?.getString("mode").toString()
        //get Views
        if (viewModel.dificulty.equals(resources.getStringArray(R.array.dificulty)[0])||
            viewModel.dificulty.equals(resources.getStringArray(R.array.dificulty)[1])){
            setContentView(R.layout.partida6)
            add6(memoryCards,memoryImages)
        }else if(viewModel.dificulty.equals(resources.getStringArray(R.array.dificulty)[2])){
            setContentView(R.layout.partida8)
            add8(memoryCards,memoryImages)
        }else if(viewModel.dificulty.equals(resources.getStringArray(R.array.dificulty)[3])){
            setContentView(R.layout.partida9)
            add9(memoryCards,memoryImages)
        }else{
            setContentView(R.layout.partida12)
            add12(memoryCards,memoryImages)
        }
        timer = findViewById(R.id.timer)
        movimentsText = findViewById(R.id.moviments)
        parellesText = findViewById(R.id.parelles)

        //Objectiu
        if (viewModel.dificulty.equals(resources.getStringArray(R.array.dificulty)[0])||
            viewModel.dificulty.equals(resources.getStringArray(R.array.dificulty)[2])||
            viewModel.dificulty.equals(resources.getStringArray(R.array.dificulty)[4])){
            viewModel.objectiu=2
            parellesText.text=("Parelles: "+viewModel.parellesFetes.toString())
        }else{
            viewModel.objectiu=3
            parellesText.text=("Ternes:"+viewModel.parellesFetes)
        }
        movimentsText.text=("Moviments:"+viewModel.moviments)

        //inicialitzar parelles i imatges
        if(viewModel.inicialitzat){
            mostarCartesActives()
        }else{
            inicialitzar()
            viewModel.inicialitzat=true
        }

        //inicialitzar timer
        if(viewModel.mode.equals(resources.getStringArray(R.array.mode)[0])){
            timer.isCountDown=false
            if (viewModel.temps!=0L){
                timer.setBase(viewModel.temps)
            }
        }else{
            timer.isCountDown=true
            if (viewModel.temps==0L){
                timer.setBase(SystemClock.elapsedRealtime()+(60000))
            }else{
                timer.setBase(viewModel.temps)
            }
        }
        timer.start()


        //Input cartes
        for (i in 0..memoryCards.count()-1){
            memoryCards[i].setOnClickListener {
                mostrarCarta(i)
            }
        }

        //Controlador temps
        if (viewModel.mode.equals(resources.getStringArray(R.array.mode)[1])){
            timer.setOnChronometerTickListener {
                if ((SystemClock.elapsedRealtime() - timer.getBase())>0L)
                {
                    acabar()
                }
            }
        }

    }
    fun add6(memoCards:ArrayList<CardView>,memoImages:ArrayList<ImageView>){
        memoCards.add(findViewById(R.id.card1))
        memoCards.add(findViewById(R.id.card2))
        memoCards.add(findViewById(R.id.card3))
        memoCards.add(findViewById(R.id.card4))
        memoCards.add(findViewById(R.id.card5))
        memoCards.add(findViewById(R.id.card6))
        memoImages.add(findViewById(R.id.image1))
        memoImages.add(findViewById(R.id.image2))
        memoImages.add(findViewById(R.id.image3))
        memoImages.add(findViewById(R.id.image4))
        memoImages.add(findViewById(R.id.image5))
        memoImages.add(findViewById(R.id.image6))
    }
    fun add8(memoCards:ArrayList<CardView>,memoImages:ArrayList<ImageView>){
        add6(memoryCards,memoryImages)
        memoCards.add(findViewById(R.id.card7))
        memoCards.add(findViewById(R.id.card8))
        memoImages.add(findViewById(R.id.image7))
        memoImages.add(findViewById(R.id.image8))
    }
    fun add9(memoCards:ArrayList<CardView>,memoImages:ArrayList<ImageView>){
        add8(memoryCards,memoryImages)
        memoCards.add(findViewById(R.id.card9))
        memoImages.add(findViewById(R.id.image9))
    }
    fun add12(memoCards:ArrayList<CardView>,memoImages:ArrayList<ImageView>){
        add9(memoryCards,memoryImages)
        memoCards.add(findViewById(R.id.card10))
        memoCards.add(findViewById(R.id.card11))
        memoCards.add(findViewById(R.id.card12))
        memoImages.add(findViewById(R.id.image10))
        memoImages.add(findViewById(R.id.image11))
        memoImages.add(findViewById(R.id.image12))
    }

    fun setDefault(){
        for (i in 0..memoryImages.count()-1){
            memoryImages[i].setImageResource(viewModel.defaultImage)
        }
    }
    fun setCartaDefault(posicio: Int){
        memoryImages[posicio].setImageResource(viewModel.defaultImage)
    }
    fun mostrarCarta(posicio: Int){
        if (!viewModel.isCartaActiva(posicio)) {
            if (viewModel.isMoveFinish()) {
                if (!viewModel.comprovarParella()) {
                    for (i in viewModel.parellaSeleccionada){
                        setCartaDefault(i)
                    }
                }
                viewModel.resetMove()
            }
            memoryImages[posicio].setImageResource(viewModel.girarCarta(posicio))

            viewModel.parellaSeleccionada.add(posicio)
            if (viewModel.isMoveFinish()){
                if (viewModel.comprovarParella()){
                    viewModel.parellesFetes++
                    if (viewModel.objectiu==3){
                        parellesText.text=("Ternes: "+viewModel.parellesFetes)
                    }else{
                        parellesText.text=("Parelles: "+viewModel.parellesFetes)
                    }
                }
                viewModel.moviments++
                isFinal()
                movimentsText.text=("Moviments: "+viewModel.moviments)
            }
        }
    }
    fun isFinal(){
        if (viewModel.isFinal()){
            if (viewModel.mode==resources.getStringArray(R.array.mode)[0]){
                acabar()
            }else{
                inicialitzar()
            }
        }

    }
    fun acabar(){
        val gameIntent = Intent(this, ResultActivity::class.java)
        gameIntent.putExtra("dificulty", viewModel.dificulty)
        gameIntent.putExtra("mode", viewModel.mode)
        gameIntent.putExtra("moviments", viewModel.moviments.toString())
        gameIntent.putExtra("parelles", viewModel.parellesFetes.toString())
        gameIntent.putExtra("temps",timer.getText())
        startActivity(gameIntent)
    }
    fun inicialitzar(){
        //Parelles i default
        if (viewModel.dificulty.equals(resources.getStringArray(R.array.dificulty)[0])||
            viewModel.dificulty.equals(resources.getStringArray(R.array.dificulty)[3])){
            viewModel.generarParelles(3)
        }else if(viewModel.dificulty.equals(resources.getStringArray(R.array.dificulty)[1])){
            viewModel.generarParelles(2)
        }else if(viewModel.dificulty.equals(resources.getStringArray(R.array.dificulty)[4])){
            viewModel.generarParelles(6)
        }else{
            viewModel.generarParelles(4)
        }
        setDefault()
    }
    fun mostarCartesActives(){
        for (i in 0..memoryImages.count()-1){
            if (viewModel.isCartaActiva(i)){
                memoryImages[i].setImageResource(viewModel.parelles[i])
            }
        }
    }

    override fun onDestroy() {
        viewModel.temps=timer.getBase()
        super.onDestroy()
    }
}