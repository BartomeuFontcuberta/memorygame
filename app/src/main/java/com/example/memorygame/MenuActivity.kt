package com.example.memorygame

import android.app.ActionBar
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.internal.ContextUtils.getActivity

class MenuActivity: AppCompatActivity() {
    lateinit var playButton: Button
    lateinit var helpButton:Button
    lateinit var dificultySpinner: Spinner
    lateinit var modeSpinner: Spinner
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_MemoryGame)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.menu)


        dificultySpinner = findViewById(R.id.dificultat)
        modeSpinner= findViewById(R.id.mode)
        playButton = findViewById(R.id.playButton)
        helpButton=findViewById(R.id.helpButton)

        ArrayAdapter.createFromResource(
            this,
            R.array.dificulty,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            dificultySpinner.adapter = adapter
        }
        ArrayAdapter.createFromResource(
            this,
            R.array.mode,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            modeSpinner.adapter = adapter
        }

        playButton.setOnClickListener {
            val gameIntent = Intent(this, GameActivity::class.java)
            gameIntent.putExtra("dificulty", dificultySpinner.getSelectedItem().toString())
            gameIntent.putExtra("mode", modeSpinner.getSelectedItem().toString())
            startActivity(gameIntent)
        }

        helpButton.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle(R.string.TitolInstruccions)
            builder.setMessage(R.string.Instruccions)
            builder.show()
        }
    }
}